select ospe.entity_id, ospe.entity_key
, content.prevver
, content.version
, content.contenttype
, content.content_status
, content.creationdate
, content.lastmoddate
, spaces.spacekey
, spaces.spacename
, content.title
, ospe.text_val
, bodycontent.body
from 
"Confluence"."dbo"."OS_PROPERTYENTRY" ospe
, content
, spaces
, bodycontent
where ospe.entity_key like 'confluence.extra.calendar%'
and content.contentid=ospe.entity_id
and content.spaceid=spaces.spaceid
and content.content_status='current' 
and bodycontent.contentid=content.contentid
and bodycontent.body like '%{calendar%'