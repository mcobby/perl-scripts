use DBI;
use XML::Simple;
use Data::Dumper;
use utf8;
use strict;
use warnings;
use autodie;

# Atlassian Calendar plugin Exporter
# Desription:
# In the beginning there was Confluence Calendars (https://studio.plugins.atlassian.com/wiki/display/CAL/Confluence+Calendar+Plugin). It was great, free but with limitations.
# Then Atlassian decided to write a new calendar application which came with many new features but, rather surprisingly for a paid plugin, there was no import facility from
# the old calendars plugin. Despite protests, an import facility was never to be, instead we were told to manually download and re-import every calendar, 100s of them as it turned out for one client.
# This problem was compounded by the fact, that due to a macro naming clash with the new team calendars, you couldn't run both side by side (did no-one think to check this?). This
# meant you had to tell every one to stop updating calendars, manually download 100s of data files, upgrade confluence and manually re-import 100s of calendars.

# I'm not a fan of manually performing the same operation 100s of times. I decided there had to be a better way.  In the end, I didn't have a time to write an importer but I did write an exporter.  
# Luckily, the old calendar plugin was a messy thing. It left behind the data for each calendar in the "OS_PROPERTYENTRY" table (even for deleted pages and calendars that were long gone). This
# meant that you could extract the data, even long after the calendar upgrade.
#
# This script connects to your Confluence database and extracts all the calendar data for all current pages and saves it as an ical file.  These ical files can then be made available for
# users to re-import themselves.  This has the advantage that calendars that are no longer in use and not transferred over. It creates a directory for every space and each ical file is named
# using the page name
#                     $spacekey-$title-$creator-$entity_key-$calName-remote.txt
#
# take this script with a warning. It worked for me and all my calendars but it's not extensively tested.  Run it, check the output & decide for yourself. 
#
# Author: Matthew Cobby
# Email: matt@andamooka.com / mcobby@gmail.com 
# Twitter: mcobby
# Date July 2012

my $dbs = "dbi:ODBC:DRIVER={SQL Server};SERVER=MYSERVER\\MYINSTANCE;Database=Confluence;";
my ($username, $password) = ('confluence', 'MYPASSWORD');
my $dbh = DBI->connect($dbs, $username, $password)  || die 'Unable to connect to the database';

my $sql = 'select ospe.entity_id, ospe.entity_key, content.prevver, content.version, content.contenttype, content.content_status, content.creationdate, content.lastmoddate, spaces.spacekey, spaces.spacename, content.title, ospe.text_val, bodycontent.body from "Confluence"."dbo"."OS_PROPERTYENTRY" ospe, content, spaces, bodycontent where ospe.entity_key like \'confluence.extra.calendar%\' and content.contentid=ospe.entity_id and content.spaceid=spaces.spaceid and content.content_status=\'current\' and bodycontent.contentid=content.contentid and bodycontent.body like \'%{calendar%\'';

# if DB Connection OK.
if (defined($dbh)) {
	# ODBC suffers from automaticlly curtailing your data if it's longer than 80 characters. 
	# This resets the limit to something more reasonable. You may need to adjust this.
	$dbh->{LongTruncOk} = 0;
	$dbh->{LongReadLen} = 1024 * 2048;
	print "LongReadLen is '", $dbh->{LongReadLen}, "'\n";
	print "LongTruncOk is ", $dbh->{LongTruncOk}, "\n";

	my $sth = $dbh->prepare($sql)
                || die "Couldn't prepare statement: " . $dbh->errstr;
    	$sth->execute || die("\n\nQUERY ERROR:\n\n$DBI::errstr");
    	$sth->execute || die("\n\nQUERY ERROR:\n\n$DBI::errstr");

	my $counter = 0;
	while (my @row = $sth->fetchrow_array()) {
        	
		# get next page with calendar data
		my ($spacekey, $spacename, $contentid, $creator, 
				$title, $lastmod, $entity_key, $caldata) = @row;

		# cleanup some of the variables for use with creating a filename
		$entity_key =~ s/^(.+)://; # calendar ID used in wiki markup
		$spacekey =~ s/\W//g;      # take out ~ from personal spaces
		$title =~ s/\W//g;         # remove non alpha-numerics
		$counter++;
		print "$counter: $spacename ($spacekey): $title: $entity_key\n";

		my $directory = './' . $spacekey;
		my $xmldirectory = $directory . '/xml';
        	unless(-e $directory or mkdir $directory) {
			die "Unable to create $directory\n";
		}
		unless(-e $xmldirectory or mkdir $xmldirectory) {
			die "Unable to create $xmldirectory\n";
		}

		# Hack alert!
		# because the Calendar XML data isn't well formed, we have to add 
		# in the missing header with the character encoding.
		# If you're only using basic UTF8 you won't need this.  If you're using 
		# international characters (or anything cut & paste from MS Word) 
		# then you will need this line to tell the XML parser how to read the data.
		$caldata = '<?xml version="1.0" encoding="iso-8859-1"?>' . "\n" . $caldata;

		# save original calendar xml data - archive purposes.
		open (CALFILE, ">$xmldirectory/$spacekey-$title-$creator-$entity_key.xml") || die $!;
		print CALFILE $caldata;
		close (CALFILE);

		# Turns xml in data structure
		my $xml = new XML::Simple (ForceArray => 1);
		my $calxml = $xml->XMLin($caldata);

		my $e;
		# for every sub-calendar associated with the calendar ID
		foreach $e (@{$calxml->{'children'}[0]->{'com.atlassian.confluence.extra.calendar.ical.model.ICalCalendar'}}) {
			my $calName =  $e->{'name'}[0];
			$calName =~ s/\W//g; # get rid on non alpha-numerics
			my $calType = $e->{'source'}[0]->{'class'};

			# if internal calendar - save as ICS file for importing to Team Calendars
			if ($calType eq 'org.randombits.source.StringSource') {
				
				# open up data file
				open (ICSFILE, ">$directory/$spacekey-$title-$creator-$entity_key-$calName.ics") || die $!;
				print "NAME: $e->{'name'}[0]\n";
				print "TYPE: $e->{'source'}[0]->{'class'}\n";

				# get ical data
				my $icsData = $e->{'source'}[0]->{'value'}[0];				
				# get rid of carriage return chars as Team Calendars import won't work with them.
				$icsData =~ s/\r//g;

				# Write data to file
				print ICSFILE "$icsData";
				close (ICSFILE);

			} elsif ($calType eq 'org.randombits.source.URLSource'){
				# elseif external calendar subscription

				# open up data file
				open (URLFILE, ">$directory/$spacekey-$title-$creator-$entity_key-$calName-remote.txt") || die $!;
				print "NAME: $e->{'name'}[0]\n";
				print "TYPE: $e->{'source'}[0]->{'class'}\n";

				# get ical data
				my $calUrl = "$e->{'source'}[0]->{'url'}[0]\n";	
				print URLFILE $calUrl;
				close (URLFILE);

			}	
		}
		print "-----------------------------------------------------\n";
	}	
	$dbh->disconnect;
	print "$counter pages with calendars."

} else {
    print "Error connecting to database: Error $DBI::err - $DBI::errstr\n";
}

